package Editor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Model {

    public void save(FileText textFile){
        try {
            Files.write(textFile.getFile(), textFile.getContent(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public IOResult<FileText> load(Path file){
        try {
            List<String> lines = Files.readAllLines(file);
            return new IOResult<>(true, new FileText(file,lines));
        } catch (IOException e) {
            e.printStackTrace();
            return new IOResult<>(false,null);
        }
    }
    public void close(){
        System.exit(0);
    }
}
