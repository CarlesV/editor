package Editor;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {
    @FXML
    private TextArea areaText;

    private FileText currentTextFile;

    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    @FXML
    private void onSave() throws IOException {
        if (currentTextFile == null) {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showSaveDialog(null);
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Texto", "*.txt"));

        } else {
            FileText textFile = new FileText(currentTextFile.getFile(), Arrays.asList(areaText.getText().split("\n")));
            model.save(textFile);
        }
    }

    @FXML
    private void onLoad() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("./"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            IOResult<FileText> io = model.load(file.toPath());

            if (io.isOk() && io.hasData()) {
                currentTextFile = io.getData();

                areaText.clear();
                currentTextFile.getContent().forEach(line -> areaText.appendText(line + "\n"));

            } else {
                System.out.println("Error");
            }
        }
    }

    @FXML
    private void onClose() {
        model.close();
    }

    @FXML
    private void onFont() {
    }

    @FXML
    private void onSizeMore() {
        double size = areaText.getFont().getSize() + 1;
        areaText.setFont(Font.font(size));
    }
    @FXML
    private void onSizeLess(){
        double size = areaText.getFont().getSize() - 1;
        areaText.setFont(Font.font(size));
    }

    @FXML
    private void onFreeSans() {
        areaText.setFont(Font.font("FreeSans", FontWeight.NORMAL,15));
    }
    @FXML
    private void onFontAwesome(){
        areaText.setFont(Font.font("FontAwesome", FontWeight.NORMAL,50));
    }

    @FXML
    private void onAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("About");
        alert.setContentText("This is a simple text editor developed by Carles Vidal");
        alert.show();
    }


    private void saveTextToFile(String content, File file) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(file);
            writer.println(content);
            writer.close();
        } catch (IOException ex) {
        }
    }
}
